pca_file="" #path for a file containing date and first N pcs for the samples to be tested and included in the reference groups, defined in config.sk
include: "config.sk"

rule inp:
	input: pca_file
	output: "PCA_sel_input"
	params: minsnp=50000
	shell: '''
cat {input[0]} | awk -v FS='\\t' -v OFS='\\t' '$1=="Removed" || NR==1 {{next}} $31>{params.minsnp} {{print $4,$12,$13,$14,$15,$31,$5}}' | sort -k7,7 -k6,6nr | uniq -f6 | cut -f-5  > {output}'''

ruleorder: test > group
rule test:
	input: pca_file
	output: "test.list"
	shell: '''
cat {input} | awk -v FS='\\t' '$1=="Projected" {{print $4}}' > {output}'''

rule group:
	input: pca_file
	output: "{group}.list"
	shell: '''
cat {input} | awk -v FS='\\t' '$2=="{wildcards.group}" {{print $4}}' > {output}'''

rule PCA_selection:
	input: "PCA_sel_input","{group}.list","test.list"
	output: "{group}.{n,[0-9]+}.{m,[0-9]+}.{e,[0-9]+}.pca_selected"
	shell: '''
Rscript ../../local/src/PCA_selection.R {input[0]} {input[1]} {input[2]} {output} {wildcards.n} {wildcards.m} {wildcards.e}'''

rule selectionsummary:
	input:
		pcasel=expand("{group}.{{par}}.pca_selected",
		group=['Yamnaya','EHG','Anatolia_N','CHG_Iran_N','WHG']),
		pca=pcafile
	output: 'PCA_selection.{par,[0-9.]+}.summary'
	shell:'''
awk -v OFS="\\t" '$1=="id" {{header=$0; next}}\
	$(NF)~/^(training_)?group$/ {{$(NF)=FILENAME; gsub("[.].*","",$(NF))}}\
	$(NF)~/^non_group$|^unassigned$/ {{$(NF)="NA"}}\
	{{print}}\
	END {{print "0"header}}' {input.pcasel} \
| sort -k2,2 | uniq | collapsesets $(awk 'NR==1 {{print NF}}' {input}) | sort | sed -r 's/;NA|NA;//' \
| translate -a -r -n <(echo -e 'training_status\\t0id'; cut -f2,4 {input.pca}) 1 > {output}'''

