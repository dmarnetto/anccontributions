#!/usr/bin/env python

from re import split, search, match,sub
from sys import stderr,stdout,stdin,exit,argv
from os.path import isfile
from itertools import product
import gzip
from os import stat
import argparse
import pysam
from multiprocessing import Pool
from warnings import warn


def safe_open(filename):
	if search('\.gz$', filename):
		f=gzip.open(filename,'rt')
	else:
		f=stdin if filename=="-" else open(filename,'r')
	return(f)

def floatOrNone(x):
	try:
		return(float(x))
	except ValueError:
		return(None)

def get1stvalue(x):
	return(list(x.values())[0])

####### defined in Patterson et al 2012 appendix A

def h(f,n):
	c = f*n
	h = (c*(n-c)) / (n*(n-1))
	return(h)




#open chr specific frequency/genotype file
def openff(filename):
	assert isfile(filename+'.tbi') or isfile(filename+'.csi'), filename+' has no tabix index'
	ff=pysam.Tabixfile(filename)
	ffhead=ff.header[0].strip().split("\t")
	return(ffhead,ff)

#computes frequency of a set from a list of indexes and a list containing fields of a ff line
def freq_comp(idxs,ffl):
	x=[float(ffl[i]) for i in idxs if floatOrNone(ffl[i]) is not None]
	try:
		return(sum(x)/len(x))
	except ZeroDivisionError: #when size of mypop is 0
		return(None)

def size_comp(idxs,ffl):
	x=[2 for i in idxs if floatOrNone(ffl[i]) is not None]
	return(sum(x))

#fstat computation in a single chr independently
def F_collector(ff_filename,bed,samples,args):
	global F
	ffhead,ff=openff(ff_filename)

	#a_idxs contains indexes for the individuals who belong to populations a
	if args.single: #fstat computation for each individual in 'samples' independently
		#dictionary with name and ff index for all samples
		a_idxs={x:[ffhead.index(x)] for x in samples.keys()}
	else:
		a_idxs={}
		for k,v in samples.items():
			try:
				a_idxs[v].append(ffhead.index(k))
			except KeyError:
				a_idxs[v]=[ffhead.index(k)]

	#b_idxs contains indexes for populations b 
	b_idxs={x:ffhead.index(x) for x in args.b_pop}
	#c_idxs alway contains an index for population c
	if args.c_pop:
		c_idxs={args.c_pop: ffhead.index(args.c_pop)}
	else:
		c_idxs=None
	#d_idxs either contains an index for population d or a list of indexes for the individuals who belong to it
	if args.stat=='f4':
		if args.d_pop:
			d_idxs={args.d_pop:ffhead.index(args.d_pop)}
		else:
			#compute frequency of all individuals in sample
			d_idxs={'all':[i for sublist in a_idxs.values() for i in sublist]}
			if args.rm_a_from_d:
				for k,v in a_idxs.items():
					d_idxs['non'+k]=list(set(d_idxs['all']).difference(set(v)))
				del(d_idxs['all'])
	else:
		d_idxs=None

	myfstat=FF_collector(args.stat,a_idxs,b_idxs,c_idxs,d_idxs,args.size)
	for bedline in bed:
		for ffline in ff.fetch(bedline[0], int(bedline[1]), int(bedline[2])):
			myfstat.addline(ffline)
	return(myfstat.tot) #tot is a dictionary with key= a tuple (a,b) and value=  a list with [tot numerator, tot denominator, snp count, a size, d name]


class FF_collector:
	def __init__(self,f,a,b,c,d,sizes):
		available_stats={	'f3':(self.f3,self.prepf3,self.formulaf3),
					'f4':(self.f4,self.prepf4,self.formulaf4),
					'fst':(self.fst,self.prepfst,self.formulafst),
					'cov_ma':(self.cov_ma,self.prepcov_ma,self.formulacov_ma)}
		self.F,self.prep,self.formula=available_stats[f]
		self.a_idxs=a
		self.b_idxs=b
		self.c_idxs=c
		self.d_idxs=d
		self.size=sizes
		self.tot={}

	def addline(self,ffline):
		ffl=ffline.strip().split("\t")
		#this prepares other stuff
		try:
			self.prep(ffl)
		except TypeError: #if some needed number is None, receives TypeError
			warn("line "+"\t".join(ffl[0:5])+"... contains an unexpected value, skipping line...")
			return
		#this computes fstat for all combinations
		for x,y in product(self.a_idxs.keys(), self.b_idxs.keys()):
			try:
				fstat=self.F(x,y)
			except TypeError: #if some needed number is None, receives TypeError
				warn("line "+"\t".join(ffl[0:5])+"... contains an unexpected value, skipping operation...")
				continue
			try:
				self.tot[(x,y)][0:3]=[x+y for x,y in zip(self.tot[(x,y)][0:3],fstat+[1])]
			except KeyError:
				self.tot[(x,y)]=fstat+[1,len(self.a_idxs[x]),self.formula(x,y)]

	def computeab(self,ffl):
		#this computes a
		a={k:freq_comp(v,ffl) for k,v in self.a_idxs.items()}
		#this computes b
		b={k:floatOrNone(ffl[v]) for k,v in self.b_idxs.items()}
		return([a,b])

	#fst(test,anc)
	def fst(s,x,y):
		num=(s.a[x]-s.b[y])**2 - h(s.a[x],s.n_a[x])/(s.n_a[x]) - h(s.b[y],s.n_b[y])/(s.n_b[y])
		den=num + h(s.a[x],s.n_a[x]) + h(s.b[y],s.n_b[y])
		return([num,den])

	def prepfst(self,ffl):
		self.a,self.b=self.computeab(ffl)
		self.n_a={k:size_comp(v,ffl) for k,v in self.a_idxs.items()}
		self.n_b={k:self.size[k] for k in self.b_idxs.keys()}

	def formulafst(self,x,y):
		return('FST('+x+','+y+')')

	#f3(test,anc;out)
	def f3(s,x,y):
		num = (s.c-s.a[x])*(s.c-s.b[y]) - h(s.c,s.n_c)/(s.n_c)
		den = (2*s.c*(1-s.c))
		return([num,den])

	def prepf3(self,ffl):
		self.a,self.b=self.computeab(ffl)
		ck=list(self.c_idxs.keys())[0]
		self.c=floatOrNone(ffl[self.c_idxs[ck]])
		self.n_c=self.size[ck]

	def formulaf3(self,x,y):
		ck=list(self.c_idxs.keys())[0]
		return('F3('+x+','+y+';'+ck+')')

	#f4(test,mod;anc,out)
	def f4(s,x,y):
		z=s.a_d_assoc[x]
		num=(s.a[x]-s.d[z])*(s.b[y]-s.c)
		den=1
		return([num,den])

	def prepf4(self,ffl):
		self.a,self.b=self.computeab(ffl)
		ck=list(self.c_idxs.keys())[0]
		self.c=floatOrNone(ffl[self.c_idxs[ck]])
		if isinstance(list(self.d_idxs.values())[0],list):
			self.d={k:freq_comp(v,ffl) for k,v in self.d_idxs.items()}
		else:
			self.d={k:floatOrNone(ffl[v]) for k,v in self.d_idxs.items()}
		if len(self.d)==1:
			dk=list(self.d.keys())[0]
			self.a_d_assoc={k:dk for k in self.a.keys()}
		else:
			self.a_d_assoc={k:'non'+k for k in self.a.keys()}

	def formulaf4(self,x,y):
		ck=list(self.c_idxs.keys())[0]
		return('F4('+x+','+self.a_d_assoc[x]+';'+y+','+ck+')')

	#covariance_modern_vs_ancient(mod,anc)
	def cov_ma(s,x,y):
		num=(s.a[x]-s.avg_a)*(s.b[y]-s.avg_b)
		den=1
		return([num,den])

	def prepcov_ma(self,ffl):
		self.a,self.b=self.computeab(ffl)
		self.avg_a=sum(self.a.values())/len(self.a)
		self.avg_b=sum(self.b.values())/len(self.b)

	def formulacov_ma(self,x,y):
		return('cov_MA('+x+','+y+')')

def main(arguments):

	parser = argparse.ArgumentParser(
		description='''
compute f-statistics according to Patterson 2012 appendix A on a bed of regions, for groups of individuals and some reference populations present in input file.
f-statistics:
	f3(c;a,b) as (c-a)(c-b) corrected for sample size of c (need to supply as option with -n 'popname' #samples)
	f4(a,d;b,c) as (a-d)(b-c)
	fst(a,b) as f2/(f2 - h_a - h_b) corrected for sample size of a and b (need to supply as option with -n 'popname' #samples)

other statistics:
	cov_ma(a,b) as (a-avg(a))(b-avg(b))

a = sets of individuals extracted from Sets file
b = ancient populations, frequencies extracted from input file, defined with --b_pop
c = outgroup population, frequency extracted from input file, defined with --c_pop
d = comparison population in F4
''',
		formatter_class=argparse.RawDescriptionHelpFormatter)
	parser.add_argument('stat', help="statistic to compute among f3, f4, fst, cov_ma")
	parser.add_argument('infile', help="Frequencies and genotypes input file")
	parser.add_argument('bedfile', help="Candidate regions bed file")
	parser.add_argument('setsfile', help="Individual sets file")
	parser.add_argument('--single', help="compute the statistics separately for each individual in 'a', i.e. each individual is a set",action='store_true')
	parser.add_argument('-n','--size',help="Give sample size for reference populations, needed in some cases, use with two arguments as in: -n <population_name>  <number_of_samples>. samples are considered diploid so multiplied for 2",action='append',nargs=2,default=[])
	parser.add_argument('-b','--b_pop', help="Reference populations to use as b in f3(c;a,b), f4(a,d;b,c), fst(a,b). At least one required, but more accepted as in -b <pop1> -b <pop2> -b <pop3> ..., to do many statistics in one run",type=str,required=True,action='append')
	parser.add_argument('-c','--c_pop', help="Reference population to use as c in f3(c;a,b), f4(a,d;b,c)",type=str,default=None)
	parser.add_argument('-d','--d_pop', help="Reference population to use as d in f4(a,d;b,c), if -d is not supplied, by default is composed by all individuals in Sets file",type=str,default=None)
	parser.add_argument('--rm_a_from_d', help="From the reference population to use as d in f4(a,d;b,c) are removed the individuals already present in a, so each test is 'a' against 'non-a' taken from the individuals in Sets file. only valid with default d",action='store_true')
	parser.add_argument('-p', help="N of parallel processes",type=int, default=1)
	args = parser.parse_args(arguments)
	
	
	emptysize={p:None for p in args.b_pop+[args.c_pop,args.d_pop]}
	emptysize.update({x[0]:int(x[1])*2 for x in args.size}) #number of alleles is diploid number*2
	args.size=emptysize
	if args.stat=='f3':
		assert args.c_pop, "population c needed to compute f3; supply it with -c"  
		assert args.size[args.c_pop], "size of population "+args.c_pop+" needed to compute unbiased f3; supply it with -n"
	if args.stat=='f4':
		assert args.c_pop, "population c needed to compute f4; supply it with -c"  
	if args.stat=='fst':
		for i in args.b_pop:
			assert args.size[i], "size of population "+i+" needed to compute unbiased fst; supply it with -n"
	if args.rm_a_from_d:
		assert not args.d_pop, "samples of 'a' can be removed from 'd' only if 'd' are all samples in Sets file, remove option -d"
		if args.single:
			warn("option --rm_a_from_d is not applied with single mode")
			args.rm_a_from_d=False
	for f in [args.setsfile,args.bedfile]:
		assert stat(f).st_size != 0, f+" is empty"

	#open bed with candidate regions
	wg_bed={}
	with safe_open(args.bedfile) as bed:
		for l in bed:
			if l.startswith('#'):
				continue
			i=l.strip().split("\t")
			try:
				wg_bed[i[0]].append(i)
			except KeyError:
				wg_bed[i[0]]=[i]
		
	#open file with trait classification
	with safe_open(args.setsfile) as tr:
		samples=dict([l.strip().split('\t') for l in tr.readlines()])
	
	#run F statistic for each chromosome independently
	pool=Pool(processes=args.p) # start p worker processes
	ffs=[sub('#CHR',x,args.infile) for x in wg_bed.keys()]
	samples_repeated=[samples for x in wg_bed.keys()]
	args_repeated=[args for x in wg_bed.keys()]
	F_inputs=list(zip(ffs,wg_bed.values(),samples_repeated,args_repeated))
	results=pool.starmap(F_collector,F_inputs)
	chr_F={k:v for k,v in zip(wg_bed.keys(),results)}
	
	# check that individuals in sets are the same in different chrs removed

	#sum chromosome results
	tot_F={}
	for thischr in chr_F.values():
		for k,v in thischr.items():
			try:
				tot_F[k][0:3]=[x+y for x,y in zip(tot_F[k][0:3],v[0:3])]
			except KeyError:
				tot_F[k]=v
			else:
				assert tot_F[k][3]==v[3] and tot_F[k][4]==v[4]
	#average results and print output
	d_pop='NA'
	if args.stat=='f4':
		d_pop=args.d_pop if args.d_pop  else 'all'
		if args.rm_a_from_d: 
			d_pop='all_non_a'
	for k,v in tot_F.items():
		#v[0] is the numerator of Fstatistic
		#v[1] is the denominator of Fstatistic
		#v[2] is the snp count
		try:
			Fstatistic=format((v[0]/v[2])/(v[1]/v[2]),'.6g') #fraction of averages
		except ZeroDivisionError:
			Fstatistic='NA'
		a="\t".join([k[0],samples[k[0]]]) if args.single else k[0] #k[0] are the names, samples[k[0]] trait classifications
		b=k[1] #k[1] the ancestry compared
		snpsize=str(v[2])
		setsize=str(v[3])
		formula=v[4]
		print("\t".join([a,b,formula,Fstatistic,snpsize,setsize]))

if __name__ == '__main__':
    exit(main(argv[1:]))

