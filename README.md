# Ancestral genomic contributions to complex traits in contemporary Europeans #

This repository contains scripts and methods used in the project: *"Ancestral genomic contributions to complex traits in contemporary Europeans"*. The paper is submitted for publication but you can find an early pre-print version at https://www.biorxiv.org/content/10.1101/2021.08.03.454888v1
