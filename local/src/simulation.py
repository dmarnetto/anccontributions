#!/usr/bin/env python
from argparse import ArgumentParser
from math import sqrt
from sys import stderr
import numpy as np
import scipy.stats
import re
import gzip
import random

def safe_open(filename):
	if re.search('\.gz$', filename):
		f=gzip.open(filename,'rt')
	else:
		f=stdin if filename=="-" else open(filename,'r')
	return(f)

def generate_index(x):
	n=-1
	for i in x:
		n+=1
		yield(n,i)

def int_or_none(x):
	try:
		return(int(x))
	except ValueError:
		return(None)

def parsevcfline(vcfline):
	chrom,pos,rsid,ref,alt,qual,filt,info,form,*gts=vcfline.rstrip('\r\n').split('\t') 
	haps=[int_or_none(y) for x in gts for y in re.split("[|/]",x)] 
	return (chrom,pos,haps)

def main():
	parser = ArgumentParser()
	parser.add_argument('countfile', type=str)
	parser.add_argument('posfile', type=str)
	parser.add_argument('vcffile', type=str)
	parser.add_argument('outprefix', type=str)
	parser.add_argument('--make_ff_file', default=None, type=str, help='does not simulate anything, just make a ff file (like the one luca gave me) for ech chromosome. Expects a pattern for the output file names including the string \'CHR\'')
	parser.add_argument('-b',dest='betacorr', default=None, type=float)
	parser.add_argument('-c',dest='causalpop', default='EUR1')
	
	args = parser.parse_args()

        #load counts
	with safe_open(args.countfile) as cf:
		pop=cf.readline().strip().split(",")
		counts=np.loadtxt(cf,delimiter=",",dtype=int)
	print("loaded counts",file=stderr,flush=True)

	#define parameters
	anc_pops=['EUR1','EUR2','EUR3']
	outgroup='AFR'
	mod_pop='MIX'
	samplesizes=[100]*5
	samplesizes[pop.index(mod_pop)]=5000

	#define freqs
	freqs=counts/(np.array(samplesizes)*2)

	if args.make_ff_file:		
		print("making ff_file",file=stderr,flush=True)
		with safe_open(args.vcffile) as vcffile:
			for line in vcffile:
				if re.match(r'^#CHROM', line):
					header= line.rstrip('\r\n').split('\t')
					break
			prevchrom=None
			n=0
			for line in vcffile:
				chrom,pos,haps=parsevcfline(line)
				indfs=[str((x+y)/2) for x,y in zip(haps[::2],haps[1::2])]
				popfs=[str(x) for x in (freqs[n])]
				if chrom!=prevchrom:
					try:
						ff_file.close()
					except UnboundLocalError:
						pass
					ff_file=open(args.make_ff_file.replace('CHR',str(chrom)),"w")
					print("\t".join(header[:2]+pop+header[9:]),file=ff_file)
				print("\t".join([chrom,pos]+popfs+indfs),file=ff_file)
				prevchrom=chrom
				n+=1
		ff_file.close()
		exit()
		

	#load coords
	with safe_open(args.posfile) as vf:
		coords=np.loadtxt(vf,delimiter="\t",dtype=int,usecols = (0,1))
	print("loaded coords",file=stderr,flush=True)

	#define parameters 2
	h2=0.67
	N=1000
	ws=50000
	wd=1000000
	chrl=10**8
	minmaf=0.1
	top_windows=50
	
	#choose causal snps
	causalsnp=[]
	#condition on snps with MIX maf>0.1
	allowedsnp=set([i for i,x in enumerate(freqs[:,pop.index(mod_pop)]) if x>minmaf and x<1-minmaf])
	#randomly choose 1 snp in the central quartiles of each window
	find_this_window_snps=generate_index(coords)
	for n in range(N):
		thisw=[]
		for i,coord in find_this_window_snps:
			x=coord[0]*chrl+coord[1]
			if x//wd == n:
				if x%wd>ws/4*1 and x%wd<ws/4*3:
					thisw.append(i)
			else:
				break
		try:
			causalsnp.append(random.choice(list(set(thisw) & allowedsnp)))
		except IndexError:
			print('Warning: '+str(len(list(set(thisw) & allowedsnp)))+" snps out of "+str(len(thisw))+" from window "+str(n*wd)+"-"+str(n*wd+ws)+" have maf>"+str(minmaf),file=stderr)
			causalsnp.append(random.choice(thisw))
	#every window has a causalsnp, now define betas
	if args.betacorr != None:
		r=args.betacorr**2
	else:
		r=np.random.uniform(0,0.25)**2
	B=np.random.normal(size=N)
	#betas are reordered and moved to match causalsnp frequency difference as requested through spearman correlation
	B.sort()
	#frequency difference from european donor population to other 2
	sfd={}
	for mypop in anc_pops:
		otheranc_pops=list(set(anc_pops)-set([mypop]))
		otherpops=[pop.index(x) for x in otheranc_pops]
		fd=freqs[causalsnp,pop.index(mypop)]-np.mean(freqs[np.ix_(causalsnp,otherpops)],1)
		sfd[mypop]=(fd-np.mean(fd))/np.std(fd)
	sfdo=np.argsort(sqrt(r)*sfd[args.causalpop]+sqrt(1-r)*np.random.normal(size=N))
	sfdr=np.argsort(sfdo)
	B=B[sfdr]
	#w is the progressive number of window, s is the progressive number of snp, or snp ID
	ss={w:[coords[s,0],coords[s,1],s,freqs[s,pop.index(mod_pop)],B[w]] for w,s in enumerate(causalsnp)}
	causalsnp_coords=set([str(x[0])+'\t'+str(x[1]) for x in ss.values()])

	# extract top n betas which will be associated to candidate windows
	top_n=top_windows*(-1)
	candidatew=sorted(np.argpartition(abs(B),top_n)[top_n:].tolist())
	
	with open(args.outprefix+".freqs","w") as freqfile:
		for mypop in anc_pops:
			spear=str(scipy.stats.spearmanr(sfd[mypop],B)[0])
			print("\t".join(["##freqdiff_beta_corr",'req:',args.causalpop,str(sqrt(r)),"real:",mypop,spear]),file=freqfile)
		print("\t".join(['#CHR','POS','ID','FREQ','BETA','HIT']+pop),file=freqfile)
		for w,s in ss.items():
			hit= 1 if w in candidatew else 0
			print("\t".join([str(x) for x in s+[hit]+list(freqs[s[2]])]),file=freqfile)
	print("printed causal snp frequencies in file"+args.outprefix+".freqs",file=stderr,flush=True)

	with open(args.outprefix+".bed",'w') as candidatefile:	
		for w in candidatew:
			w_chr=w*wd//chrl
			w_start= w*wd%chrl
			bedline=[w_chr,w_start,w_start+ws]
			print("\t".join([str(x) for x in bedline]),file=candidatefile)

	print("printed candidate regions in file "+args.outprefix+".bed",file=stderr,flush=True)
	# compute phenotype
	with safe_open(args.vcffile) as vcffile:
		for line in vcffile:
			if re.match(r'^#CHROM', line):
				header= line.rstrip('\r\n').split('\t')
				samples=header[9:]
				break
		n=0
		haptable=np.zeros(shape=(N,sum(samplesizes)*2),dtype=int)
		for line in vcffile:
			vcfcoord=re.match(r'^([^\t]+\t[0-9]+)', line)
			if vcfcoord.group(1) in causalsnp_coords:
				haps=parsevcfline(line)[2]
				haptable[n]=haps
				n+=1
	print("loaded vcf",file=stderr,flush=True)
	hap_ps=np.dot(B,haptable)
	ps=hap_ps[0::2]+hap_ps[1::2]
	sps=(ps-np.mean(ps))/np.std(ps)
	env=np.random.normal(size=sum(samplesizes))
	p=sqrt(h2)*sps + sqrt(1-h2)*env
	with open(args.outprefix+".traits",'w') as traitfile:
		for mypop in anc_pops:
			test=[i for i,x in enumerate(samples) if re.match(mypop, x)]
			otherpattern='|'.join(set(anc_pops)-set([mypop]))
			other=[i for i,x in enumerate(samples) if re.match(otherpattern, x)]
			direction='higher' if np.median([p[x] for x in test])-np.median([p[x] for x in other]) > 0 else 'lower'
			mwu=scipy.stats.mannwhitneyu(p[test],p[other],alternative='two-sided')
			print("\t".join(['##mannwhitneyu:',mypop,str(mwu.statistic),str(mwu.pvalue),direction]),file=traitfile)
		print("\t".join(['#ID','PS','trait']),file=traitfile)
		for ind,ps,trait in zip(samples,sps,p):
			print("\t".join([ind,str(ps),str(trait)]),file=traitfile)
	print("printed trait values in file "+args.outprefix+".traits"+"\ndone.",file=stderr,flush=True)

if __name__ == '__main__':
	main()


